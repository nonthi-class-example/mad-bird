using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float _destroyImpactMagnitude = 5f;

    [SerializeField]
    private float _killY = -10f;

    private Animator _animator;

    private Collider2D _collider;

    private bool _killed = false;
    public bool Killed => _killed;

    private GameFlowController _gameFlowController;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (transform.position.y < _killY)
        {
            Kill();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<Bird>())
        {
            Kill();
        }

        if (collision.relativeVelocity.magnitude > _destroyImpactMagnitude)
        {
            Kill();
        }
    }

    public void Initialize(GameFlowController gameFlowController)
    {
        _gameFlowController = gameFlowController;
    }

    private void Kill()
    {
        if (_killed) return;
        _killed = true;
        _animator.SetTrigger("Killed");
    }

    private void HandleKilledAnimationFinished()
    {
        Destroy(gameObject);

        _gameFlowController.NotifyEnemyDeath(this);
    }
}
