using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameFlowController : MonoBehaviour
{
    private List<Enemy> _enemies;

    private void Start()
    {
        _enemies = new List<Enemy>(FindObjectsOfType<Enemy>());
        foreach (Enemy anEnemy in _enemies)
        {
            anEnemy.Initialize(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void NotifyEnemyDeath(Enemy killedEnemy)
    {
        _enemies.Remove(killedEnemy);
        if (_enemies.Count == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
