using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField]
    private float _launchPower = 5f;

    [SerializeField]
    private float _maxDragDistance = 2f;

    private Rigidbody2D _rigidbody;
    private Vector3 _startingPosition;
    private LineRenderer _dragLineRenderer;

    private bool _launched;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _dragLineRenderer = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        _startingPosition = transform.position;
        _dragLineRenderer.SetPosition(0, transform.position);
        _dragLineRenderer.enabled = false;
    }

    private void OnMouseDrag()
    {
        if (_launched) return;

        Vector3 destination = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        destination.z = 0f;
        if (Vector2.Distance(_startingPosition, destination) > _maxDragDistance)
        {
            destination = Vector3.MoveTowards(_startingPosition, destination, _maxDragDistance);
        }

        transform.position = destination;
        _rigidbody.velocity = Vector2.zero;

        _dragLineRenderer.SetPosition(1, transform.position);
        _dragLineRenderer.enabled = true;
    }

    private void OnMouseUp()
    {
        if (_launched) return;

        _launched = true;

        Vector3 launchVector = _startingPosition - transform.position;
        _rigidbody.AddForce(launchVector * _launchPower, ForceMode2D.Impulse);
        _rigidbody.gravityScale = 1f;
        _dragLineRenderer.enabled = false;
    }
}
