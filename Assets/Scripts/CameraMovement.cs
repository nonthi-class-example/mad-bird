using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 3f;
    [SerializeField] private Collider2D _floorCollider;
    [SerializeField] private float _margin = 1f;

    private Camera _camera;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            float camLeft = _camera.ViewportToWorldPoint(new Vector2(0f, 0f)).x;
            if (camLeft > _floorCollider.bounds.min.x - _margin)
            {
                transform.position -= _speed * Time.deltaTime * Vector3.right;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            float camRight = _camera.ViewportToWorldPoint(new Vector2(1f, 0f)).x;
            if (camRight < _floorCollider.bounds.max.x + _margin)
            {
                transform.position += _speed * Time.deltaTime * Vector3.right;
            }
        }
    }
}
